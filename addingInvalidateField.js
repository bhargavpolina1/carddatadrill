const cardDetails = require("./data.js");

let addingIsValidField = cardDetails
  .map((eachCard) => {
    let randomNumber = (Math.random() * 10000).toString();
    let cvvNumber = Number(randomNumber.slice(0, 3));
    eachCard["CVV"] = cvvNumber;
    return eachCard;
  })

  .map((eachCard) => {
    eachCard["is_valid"] = null;
    return eachCard;
  });

console.log(addingIsValidField);
