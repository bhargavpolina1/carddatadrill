const cardDetails = require("./data.js");

let cardsBeforeJune = cardDetails.filter((eachCardDetails) => {
  let cardIssue = eachCardDetails.issue_date;
  let cardIssueArray = cardIssue.split("/");
  let cardMonth = cardIssueArray[0];
  return parseInt(cardMonth) < 6;
});

console.log(cardsBeforeJune);
