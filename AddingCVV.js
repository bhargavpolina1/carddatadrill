const cardDetails = require("./data.js");

let addCvv = cardDetails.map((eachCard) => {
  let randomNumber = (Math.random() * 10000).toString();
  let cvvNumber = Number(randomNumber.slice(0, 3));
  eachCard["CVV"] = cvvNumber;
  return eachCard;
});

console.log(addCvv);
