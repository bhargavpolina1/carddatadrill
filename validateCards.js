const cardDetails = require("./data.js");

let addingIsValidField = cardDetails
  .map((eachCard) => {
    let randomNumber = (Math.random() * 10000).toString();
    let cvvNumber = Number(randomNumber.slice(0, 3));
    eachCard["CVV"] = cvvNumber;
    return eachCard;
  })
  .map((eachCard) => {
    eachCard["is_valid"] = null;
    return eachCard;
  })
  .map((eachCardEntry) => {
    let cardIssue = eachCardEntry.issue_date;
    let cardIssueArray = cardIssue.split("/");
    let cardMonth = cardIssueArray[0];
    if (cardMonth < 3) {
      eachCardEntry["is_valid"] = "invalid";
    } else {
      eachCardEntry["is_valid"] = "valid";
    }
    return eachCardEntry;
  });

console.log(addingIsValidField);
